from lib import preprocessing

def main():
    data_dir = '/srv/tempdd/egermani/hcp_many_pipelines'
    out_dir = '/srv/tempdd/egermani/hcp_many_pipelines_preprocess'
    resolution = 4
    preprocessing.preprocessing(data_dir, out_dir, resolution)

if __name__ == '__main__':
    main()
           